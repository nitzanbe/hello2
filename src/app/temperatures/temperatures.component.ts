import { Observable } from 'rxjs';
import { WeatherService } from './../weather.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Weather } from '../interfaces/weather';

@Component({
  selector: 'temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {
  city:string;
  temperature:number;
  image:string;
  country:string;
  lat:number;
  lon:number;
  description:string;
  weatherData$:Observable<Weather>;
  hasError:Boolean = false;
  errorMessage:string;
  constructor(private route:ActivatedRoute, private weatherService:WeatherService) { }

  ngOnInit(): void {
    this.city = this.route.snapshot.params.city;
    this.weatherData$ = this.weatherService.searchWeatherData(this.city);
    this.weatherData$.subscribe(
      data => {
        this.temperature = data.temperature;
        this.image = data.image;
        this.country = data.country;
        this.lat = data.lat;
        this.lon = data.lon;
        this.description = data.description;
      },
      error =>{
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message;
      }
    )
  }

}
