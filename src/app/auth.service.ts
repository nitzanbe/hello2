import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {User} from './interfaces/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user:Observable<User | null>;

  login(email:string, password:string){
    return this.afAuth.signInWithEmailAndPassword(email, password);
  }

  register(email:string, password:string){
    return this.afAuth.createUserWithEmailAndPassword(email, password);
  }

  logout(){
    this.afAuth.signOut();
  }

  getUser():Observable<User | null> {
    return this.user;
  }

  constructor(private afAuth:AngularFireAuth, private router:Router) {
    this.user = this.afAuth.authState;
   }
}
