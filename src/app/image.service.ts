import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private path:string = 'https://firebasestorage.googleapis.com/v0/b/hello-jce-nitzan.appspot.com/o/'; 
  public images:string[] = [];
  constructor() {
    this.images[0] = this.path + 'biz.jpeg' + '?alt=media&token=70e7a702-de7d-4e39-8aa6-a5f9888c7e89';
    this.images[1] = this.path + 'entertaiment.jpeg' + '?alt=media&token=fd69e484-8909-419f-8afe-0ff5f1049c05';
    this.images[2] = this.path + 'politics.jpeg' + '?alt=media&token=053a92ef-4c3b-47b1-b43c-159b6d8a833a';
    this.images[3] = this.path + 'sport.jpeg' + '?alt=media&token=2215c0f2-1500-40d2-bb1f-e97a0822c297'; 
    this.images[4] = this.path + 'tech.jpeg' + '?alt=media&token=ff8fba3e-2921-4c71-bf47-5c14f1e3c084';
   }
}
