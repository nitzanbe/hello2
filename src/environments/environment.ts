// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBGHGGqJGpJBSh9qhs8rPrv7gYxhw-cHBQ",
    authDomain: "hello-jce-nitzan.firebaseapp.com",
    databaseURL: "https://hello-jce-nitzan.firebaseio.com",
    projectId: "hello-jce-nitzan",
    storageBucket: "hello-jce-nitzan.appspot.com",
    messagingSenderId: "815044130288",
    appId: "1:815044130288:web:24af6ece2ca953de0d5935"
  }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
